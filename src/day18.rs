use std::fmt;

use crate::common;

pub fn solve() -> Result<(), String> {
    let snail_numbers: Vec<SnailNumber> = common::read_input()
        .iter()
        .map(|line| parse_snail_number(line.as_bytes()))
        .collect();

    let mut result = snail_numbers[0].clone();
    result.reduce();
    print!("    {}\n", result);
    for snail_number in snail_numbers.iter().skip(1) {
        print!("+   {}\n", snail_number);
        result = result.add(snail_number.clone());
        println!("~   {}", result);
        result.reduce();
        println!("=   {}", result);
    }

    println!("sum: {}", result);
    println!("magnitude: {}", result.magnitude());

    let mut max_magnitude = -1;
    for i in 0..snail_numbers.len() {
        for j in 0..snail_numbers.len() {
            if i != j {
                let left = snail_numbers[i].clone();
                let right = snail_numbers[j].clone();
                let mut sum = left.add(right);
                sum.reduce();
                let magnitude = sum.magnitude();
                if magnitude > max_magnitude {
                    println!("{} ({})", sum, magnitude);
                    max_magnitude = magnitude;
                }
            }
        }
    }

    println!("max: {}", max_magnitude);

    Ok(())
}

#[derive(Debug, Clone)]
enum SnailNumber {
    Single(i32),
    Pair {
        left: Box<SnailNumber>,
        right: Box<SnailNumber>,
    },
}

type ExplodeResult = ((bool, i32), (bool, i32));

impl SnailNumber {
    fn add(self, other: SnailNumber) -> SnailNumber {
        SnailNumber::Pair {
            left: Box::from(self),
            right: Box::from(other),
        }
    }

    fn reset(&mut self) {
        match self {
            SnailNumber::Pair { left, right } => {
                left.reset();
                right.reset();
            }
            SnailNumber::Single(value) => *value = 0,
        }
    }

    fn reduce(&mut self) {
        while self.explode() || self.split() {
            // TODO: modifications in the while condition
        }
    }

    fn explode(&mut self) -> bool {
        self._explode(0).is_some()
    }

    fn _explode(&mut self, level: i32) -> Option<ExplodeResult> {
        match self {
            SnailNumber::Pair { left, right } => {
                if level == 4 {
                    if let SnailNumber::Single(left) = **left {
                        if let SnailNumber::Single(right) = **right {
                            *self = SnailNumber::Single(0);
                            Some(((false, left), (false, right)))
                        } else {
                            panic!("unexpected right: {}", self)
                        }
                    } else {
                        panic!("unexpected left: {}", self)
                    }
                } else {
                    if let Some(result) = left._explode(level + 1) {
                        if result.1 .0 {
                            Some(result)
                        } else {
                            right.push_left(result.1 .1);
                            Some((result.0, (true, result.1 .1)))
                        }
                    } else if let Some(result) = right._explode(level + 1) {
                        if result.0 .0 {
                            Some(result)
                        } else {
                            left.push_right(result.0 .1);
                            Some(((true, result.0 .1), result.1))
                        }
                    } else {
                        None
                    }
                }
            }
            _ => None,
        }
    }

    fn push_left(&mut self, pushed: i32) {
        match self {
            SnailNumber::Pair { left, right: _ } => left.push_left(pushed),
            SnailNumber::Single(value) => *value += pushed,
        }
    }

    fn push_right(&mut self, pushed: i32) {
        match self {
            SnailNumber::Pair { left: _, right } => right.push_right(pushed),
            SnailNumber::Single(value) => *value += pushed,
        }
    }

    fn split(&mut self) -> bool {
        match self {
            SnailNumber::Pair { left, right } => left.split() || right.split(),
            SnailNumber::Single(value) => {
                if *value > 9 {
                    let left = *value / 2;
                    *self = SnailNumber::Pair {
                        left: Box::new(SnailNumber::Single(left)),
                        right: Box::new(SnailNumber::Single(*value - left)),
                    };
                    true
                } else {
                    false
                }
            }
        }
    }

    fn magnitude(&self) -> i32 {
        match self {
            SnailNumber::Pair { left, right } => 3 * left.magnitude() + 2 * right.magnitude(),
            SnailNumber::Single(value) => *value,
        }
    }
}

impl fmt::Display for SnailNumber {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            SnailNumber::Single(value) => write!(f, "{}", value),
            SnailNumber::Pair { left, right } => write!(f, "[{},{}]", left, right),
        }
    }
}

fn parse_snail_number(bytes: &[u8]) -> SnailNumber {
    if bytes[0] >= '0' as u8 && bytes[0] <= '9' as u8 {
        let single_str = String::from_utf8(bytes.into()).unwrap();
        SnailNumber::Single(single_str.parse().unwrap())
    } else {
        let mut p = 0;
        let mut d = 0;

        while d != 1 || bytes[p] != ',' as u8 {
            d += match bytes[p] as char {
                '[' => 1,
                ']' => -1,
                _ => 0,
            };

            p += 1;
        }

        SnailNumber::Pair {
            left: Box::new(parse_snail_number(&bytes[1..p])),
            right: Box::new(parse_snail_number(&bytes[p + 1..bytes.len() - 1])),
        }
    }
}
