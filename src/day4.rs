pub fn solve() -> Result<(), String> {
    let input = crate::common::read_input();

    let drawn_numbers: Vec<i32> = input[0].split(",").flat_map(|val| val.parse()).collect();

    let mut boards: Vec<Board> = read_boards(&input[1..]);
    let mut score = 0;

    'outer: for drawn_number in &drawn_numbers {
        for board in &mut boards {
            match mark_board(board, *drawn_number) {
                Some(board_score) => {
                    score = board_score;
                    break 'outer;
                }
                None => {}
            }
        }
    }

    println!("score: {}", score);

    let mut score = 0;
    let mut excluded = vec![false; boards.len()];

    for drawn_number in &drawn_numbers {
        for index in 0..boards.len() {
            if excluded[index] {
                continue;
            }

            match mark_board(&mut boards[index], *drawn_number) {
                Some(board_score) => {
                    score = board_score;
                    excluded[index] = true;
                }
                None => {}
            }
        }
    }

    println!("score: {}", score);

    Ok(())
}

type Row = [i32; 5];
type Board = [Row; 5];

fn read_boards(input: &[String]) -> Vec<Board> {
    let mut boards = vec![];

    for index in 0..input.len() / 6 {
        let mut board: Board = [[0; 5]; 5];
        for row in 0..5 {
            read_row(&input[index * 6 + row + 1], &mut board[row]);
        }
        boards.push(board);
    }

    boards
}

fn read_row(input: &str, row: &mut Row) {
    let columns: Vec<i32> = input
        .split(" ")
        .filter(|val| !val.is_empty())
        .flat_map(|val| val.parse())
        .collect();

    for c in 0..5 {
        row[c] = columns[c];
    }
}

fn mark_board(board: &mut Board, val: i32) -> Option<i32> {
    for i in 0..5 {
        for j in 0..5 {
            if board[i][j] == val {
                board[i][j] = -1;
            }
        }
    }

    for i in 0..5 {
        let mut count = 0;

        for j in 0..5 {
            if board[i][j] == -1 {
                count += 1;
            }
        }

        if count == 5 {
            return Some(score_board(board) * val);
        }
    }

    for j in 0..5 {
        let mut count = 0;

        for i in 0..5 {
            if board[i][j] == -1 {
                count += 1;
            }
        }

        if count == 5 {
            return Some(score_board(board) * val);
        }
    }

    None
}

fn score_board(board: &Board) -> i32 {
    let mut score = 0;

    for i in 0..5 {
        for j in 0..5 {
            if board[i][j] != -1 {
                score += board[i][j];
            }
        }
    }

    score
}
