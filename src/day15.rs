use crate::common;

use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::collections::HashMap;

#[derive(Debug)]
struct Index(usize, usize, isize);

pub fn solve() -> Result<(), String> {
    let grid: Vec<Vec<isize>> = common::read_input()
        .iter()
        .map(|line| {
            line.split("")
                .flat_map(|cell| cell.parse())
                .collect::<Vec<isize>>()
        })
        .collect();

    find_cost(&grid, 5);

    Ok(())
}

fn find_cost(grid: &Vec<Vec<isize>>, expand: usize) {
    let mut costs = vec![vec![(0, 0, 0); grid[0].len() * expand]; grid.len() * expand];
    let mut heap = BinaryHeap::new();

    heap.push(Index(0, 0, 0));

    while let Some(Index(i, j, cost)) = heap.pop() {
        if costs[j][i].2 > 0 {
            continue;
        }

        costs[j][i] = (i, j, -cost);

        if j == costs.len() - 1 && i == costs[0].len() - 1 {
            println!("cost: {:?}", -cost);
            break;
        }

        if j > 0 {
            heap.push(Index(i, j - 1, cost - lookup(&grid, i, j - 1)));
        }

        if j < costs.len() - 1 {
            heap.push(Index(i, j + 1, cost - lookup(&grid, i, j + 1)));
        }

        if i > 0 {
            heap.push(Index(i - 1, j, cost - lookup(&grid, i - 1, j)));
        }

        if i < costs[0].len() - 1 {
            heap.push(Index(i + 1, j, cost - lookup(&grid, i + 1, j)));
        }
    }
}

fn lookup(grid: &Vec<Vec<isize>>, i: usize, j: usize) -> isize {
    let replica_i = (i / grid[0].len()) as isize;
    let replica_j = (j / grid.len()) as isize;

    ((grid[j % grid.len()][i % grid[0].len()] + replica_i + replica_j - 1) % 9) + 1
}

impl PartialEq for Index {
    fn eq(&self, other: &Self) -> bool {
        return self.2 == other.2;
    }
}

impl Eq for Index {}

impl PartialOrd for Index {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.2.cmp(&other.2))
    }
}

impl Ord for Index {
    fn cmp(&self, other: &Self) -> Ordering {
        self.2.cmp(&other.2)
    }
}
