use crate::common;

pub fn solve() -> Result<(), String> {
    let crabs: Vec<usize> = common::read_input()
        .iter()
        .flat_map(|val| val.split(",").into_iter())
        .flat_map(|val| val.parse())
        .collect();

    let max_horizontal = crabs.iter().max();

    if let Some(max_horizontal) = max_horizontal {
        let l = *max_horizontal + 1;
        let mut positions = vec![0; l];

        for crab in &crabs {
            positions[*crab] += 1;
        }

        let mut right = vec![0; l];
        let mut left = vec![0; l];

        let mut cost_left = vec![0; l];
        let mut cost_right = vec![0; l];

        for i in 1..l {
            left[i] = left[i - 1] + positions[i - 1];
            right[l - i - 1] = right[l - i] + positions[l - i];

            cost_left[i] = cost_left[i - 1] + left[i - 1] + positions[i - 1];
            cost_right[l - i - 1] = cost_right[l - i] + right[l - i] + positions[l - i];
        }

        let mut min_cost = cost_left[0] + cost_right[0];
        let mut min_p = 0;

        for p in 1..l {
            let cost = cost_left[p] + cost_right[p];
            if cost < min_cost {
                min_p = p;
                min_cost = cost;
            }
        }

        println!("cost {} at {}", min_cost, min_p);

        let mut new_cost = vec![0; l];
        for i in 0..l {
            for j in 0..l {
                let n = (j as i32 - i as i32).abs();
                let c = (n * (n + 1)) / 2;
                new_cost[i] += c * positions[j];
            }
        }

        let min_new_cost = new_cost.iter().min();
        println!("new cost {}", min_new_cost.unwrap_or(&0));
    } else {
        println!("No elements!")
    }

    Ok(())
}
