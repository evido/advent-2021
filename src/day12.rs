use crate::common;

pub fn solve() -> Result<(), String> {
    let input: Vec<Vec<String>> = common::read_input()
        .iter()
        .map(|line| line.split("-").map(str::to_owned).collect::<Vec<String>>())
        .collect();

    let mut graph = DiGraph::new();

    for edge in &input {
        let from = match graph.find_node(&edge[0]) {
            Some(from) => from,
            None => graph.add_node(&edge[0]),
        };

        let to = match graph.find_node(&edge[1]) {
            Some(to) => to,
            None => graph.add_node(&edge[1]),
        };

        graph.add_bi_edge(from, to);
    }

    let start = graph.find_node("start").unwrap();
    let end = graph.find_node("end").unwrap();

    let mut paths = vec![vec![start]];

    loop {
        let mut modified = 0;
        let mut new_paths = vec![];

        for path in &mut paths {
            let last = path.last().unwrap();

            if *last == end {
                new_paths.push(path.clone());
                continue;
            }

            let mut path_modified = 0;
            for edge in &graph.get_edges(*last) {
                if start == edge.1 {
                    continue;
                }

                let path_has_small_visited_twice = path
                    .iter()
                    .filter(|el| {
                        let name = graph.name(**el);
                        !is_big(name)
                    })
                    .any(|el| path.iter().filter(|sub_el| *sub_el == el).count() == 2);

                let path_contains = path.contains(&edge.1);
                let is_big = is_big(graph.name(edge.1));

                if !path_has_small_visited_twice || !path_contains || is_big {
                    let mut new_path = path.clone();
                    new_path.push(edge.1);
                    path_modified += 1;
                    new_paths.push(new_path);
                }
            }

            modified += path_modified;
        }

        paths = new_paths;

        if modified == 0 {
            break;
        }
    }

    paths.iter().for_each(|path| show_path(&graph, path));
    println!("paths: {}", paths.len());

    Ok(())
}

fn show_path(graph: &DiGraph, path: &Vec<usize>) {
    for step in path {
        let name = graph.name(*step);
        print!("{} -> ", name);
    }
    println!("()");
}

fn is_big(name: &str) -> bool {
    let c = name.as_bytes()[0];
    c < 'a' as u8
}

struct DiGraph {
    edges: Vec<(usize, usize)>,
    nodes: Vec<String>,
}

impl DiGraph {
    fn new() -> DiGraph {
        DiGraph {
            edges: vec![],
            nodes: vec![],
        }
    }

    fn get_edges(&self, node: usize) -> Vec<(usize, usize)> {
        let mut edges = vec![];
        for edge in &self.edges {
            if edge.0 == node {
                edges.push(*edge);
            }
        }
        edges
    }

    fn find_node(&self, name: &str) -> Option<usize> {
        self.nodes.iter().position(|node| node == name)
    }

    fn add_bi_edge(&mut self, from: usize, to: usize) {
        self.edges.push((from, to));
        self.edges.push((to, from));
    }

    fn add_node(&mut self, name: &str) -> usize {
        self.nodes.push(name.to_owned());
        self.nodes.len() - 1
    }

    fn name(&self, node: usize) -> &str {
        &self.nodes[node]
    }
}
