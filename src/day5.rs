use std::collections::HashSet;

pub fn solve() -> Result<(), String> {
    let vents = read_vents();

    solve_grid(&vents, false);
    solve_grid(&vents, true);
    solve_smart(&vents);

    Ok(())
}

fn solve_grid(vents: &[Vent], include_diagonal: bool) {
    let mut grid = vec![vec![0_i32; 999]; 999];

    for vent in vents {
        let normalized = normalize(vent);
        if is_horizontal(vent) {
            for i in normalized.0 .0..normalized.1 .0 + 1 {
                grid[normalized.0 .1 as usize][i as usize] += 1;
            }
        } else if is_vertical(vent) {
            for i in normalized.0 .1..normalized.1 .1 + 1 {
                grid[i as usize][normalized.0 .0 as usize] += 1;
            }
        } else if include_diagonal {
            let sx = if vent.1 .0 > vent.0 .0 { 1 } else { -1 };
            let sy = if vent.1 .1 > vent.0 .1 { 1 } else { -1 };

            for i in 0..(vent.1 .0 - vent.0 .0).abs() + 1 {
                grid[(vent.0 .1 + i * sy) as usize][(vent.0 .0 + i * sx) as usize] += 1;
            }
        }
    }

    let mut count = 0;
    for row in &grid {
        for col in row {
            if *col > 1 {
                count += 1;
            }
        }
    }

    println!("number of overlaps: {}", count);
}

fn solve_smart(vents: &[Vent]) {
    let mut overlapping: HashSet<Point> = HashSet::new();

    for i in 0..vents.len() {
        let vent = &vents[i];

        if !is_horizontal(vent) && !is_vertical(vent) {
            continue;
        }

        for j in 0..i {
            let other = &vents[j];
            if !is_horizontal(other) && !is_vertical(other) {
                continue;
            }

            for point in find_overlapping(vent, other) {
                overlapping.insert(point);
            }
        }
    }

    println!("number of overlaps: {}", overlapping.len());
}

fn find_overlapping(vent: &Vent, other: &Vent) -> Vec<Point> {
    let mut points = vec![];
    let vent = &normalize(vent);
    let other = &normalize(other);

    if is_horizontal(vent) && is_horizontal(other) {
        if vent.0 .1 == other.0 .1 {
            let h1;
            let h2;

            if vent.0 .0 <= other.0 .0 {
                h1 = vent;
                h2 = other;
            } else {
                h1 = other;
                h2 = vent;
            }

            for i in h2.0 .0..h1.1 .0 + 1 {
                points.push(Point(i, h1.0 .1));
            }
        }
    } else if is_vertical(vent) && is_vertical(other) {
        if vent.0 .0 == other.0 .0 {
            let v1;
            let v2;

            if vent.0 .1 <= other.0 .1 {
                v1 = vent;
                v2 = other;
            } else {
                v1 = other;
                v2 = vent;
            }

            for i in v2.0 .1..v1.1 .1 + 1 {
                points.push(Point(v1.0 .0, i));
            }
        }
    } else {
        let h;
        let v;

        if is_vertical(vent) {
            v = vent;
            h = other;
        } else {
            v = other;
            h = vent;
        }

        if h.0 .0 <= v.0 .0 && v.0 .0 <= h.1 .0 && v.0 .1 <= h.0 .1 && h.0 .1 <= v.1 .1 {
            points.push(Point(v.0 .0, h.0 .1));
        }
    }

    points
}

fn is_horizontal(vent: &Vent) -> bool {
    vent.0 .1 == vent.1 .1
}

fn is_vertical(vent: &Vent) -> bool {
    vent.0 .0 == vent.1 .0
}

fn read_vents() -> Vec<Vent> {
    let input = crate::common::read_input();
    let mut vents = vec![];

    for raw in input {
        let vent = read_vent(&raw);
        vents.push(vent);
    }

    vents
}

fn read_vent(raw: &str) -> Vent {
    let parts: Vec<i32> = raw
        .split("->")
        .flat_map(|v| v.trim().split(","))
        .flat_map(|v| v.parse::<i32>())
        .collect();

    Vent(Point(parts[0], parts[1]), Point(parts[2], parts[3]))
}

fn normalize(vent: &Vent) -> Vent {
    if is_horizontal(vent) && vent.0 .0 > vent.1 .0 {
        Vent(vent.1, vent.0)
    } else if is_vertical(vent) && vent.0 .1 > vent.1 .1 {
        Vent(vent.1, vent.0)
    } else {
        Vent(vent.0, vent.1)
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct Point(i32, i32);

#[derive(Debug)]
struct Vent(Point, Point);
