use crate::common;

pub fn solve() -> Result<(), String> {
    let input: Vec<String> = common::read_input();

    let mut score = 0;
    let mut completion_costs = vec![];

    'outer: for line in input {
        let mut stack = vec![];

        for c in line.as_bytes() {
            let c = *c as char;
            match c {
                '(' | '{' | '[' | '<' => stack.push(c),
                ')' | '}' | ']' | '>' => {
                    if let Some(top) = stack.pop() {
                        if c == ')' && top == '('
                            || c == '}' && top == '{'
                            || c == ']' && top == '['
                            || c == '>' && top == '<'
                        {
                        } else {
                            score += match c {
                                ')' => 3,
                                ']' => 57,
                                '}' => 1197,
                                '>' => 25137,
                                _ => return Err("unexpected closing bracket".to_owned()),
                            };
                            continue 'outer;
                        }
                    } else {
                        return Err(format!("stack empty"));
                    }
                }
                _ => return Err(format!("unrecognized token: {}", c)),
            }
        }

        let mut completion_cost: i64 = 0;
        for c in stack.iter().rev() {
            completion_cost *= 5;
            completion_cost += match c {
                '(' => 1,
                '[' => 2,
                '{' => 3,
                '<' => 4,
                _ => return Err("unexpected token".to_owned()),
            }
        }
        completion_costs.push(completion_cost);
    }

    println!("score: {}", score);
    completion_costs.sort();
    let completion_cost = completion_costs[completion_costs.len() / 2];
    println!("completion cost: {}", completion_cost);

    Ok(())
}
