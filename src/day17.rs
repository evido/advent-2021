use crate::common;

#[derive(Debug)]
struct Area(isize, isize, isize, isize);

pub fn solve() -> Result<(), String> {
    let input = common::read_input();
    let area = parse_area(&input[0]);
    let mut gymax = 0;
    let mut hit = 0;

    for idx in -1000..1000 {
        for idy in -1000..1000 {
            let mut x = 0;
            let mut y = 0;
            let mut dx = idx;
            let mut dy = idy;
            let mut ymax = 0;

            loop {
                if x > area.2 {
                    break;
                }

                if dx == 0 && x < area.0 {
                    break;
                }

                if dy < 0 && y < area.1 {
                    break;
                }

                x += dx;
                y += dy;

                dy -= 1;
                if dx > 0 {
                    dx -= 1;
                }

                if y > ymax {
                    ymax = y;
                }

                if x >= area.0 && x <= area.2 && y >= area.1 && y <= area.3 {
                    hit += 1;

                    if ymax > gymax {
                        gymax = ymax;
                        println!("{} for ({}, {})", gymax, idx, idy);
                    }
                    break;
                }
            }
        }
    }

    println!("hit: {}", hit);

    println!("{:?}", area);

    Ok(())
}

fn parse_area(line: &str) -> Area {
    let line_bytes = line.as_bytes();

    let x_start = line_bytes.iter().position(|c| *c == '=' as u8).unwrap() + 1;
    let x_end = line_bytes.iter().position(|c| *c == ',' as u8).unwrap();

    let x_range = parse_range(&line_bytes[x_start..x_end]);

    let y_start = line_bytes
        .iter()
        .skip(x_end + 1)
        .position(|c| *c == '=' as u8)
        .unwrap()
        + 1;

    let y_range = parse_range(&line_bytes[x_end + 1 + y_start..]);

    Area(x_range.0, y_range.0, x_range.1, y_range.1)
}

fn parse_range(value: &[u8]) -> (isize, isize) {
    let value = String::from_utf8(value.into()).unwrap();
    let parts: Vec<&str> = value.split("..").collect();
    (parts[0].parse().unwrap(), parts[1].parse().unwrap())
}
