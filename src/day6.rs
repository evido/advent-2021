use crate::common;

pub fn solve() -> Result<(), String> {
    let input: Vec<i32> = common::read_input()
        .iter()
        .flat_map(|val| val.split(","))
        .flat_map(|val| val.parse())
        .collect();

    println!("Total Fish: {}", simulate(&input, 80));
    println!("Total Fish: {}", simulate(&input, 256));
    println!(
        "Total Fish (population): {}",
        simulate_population(&input, 80).len()
    );

    Ok(())
}

fn simulate_population(fishes: &Vec<i32>, days: i32) -> Vec<i32> {
    let mut simulated = fishes.clone();

    for _ in 0..days {
        let mut new_fishes = 0;
        for timer in &mut simulated {
            if *timer == 0 {
                *timer = 6;
                new_fishes += 1;
            } else {
                *timer -= 1;
            }
        }
        for _ in 0..new_fishes {
            simulated.push(8);
        }
    }

    simulated
}

fn simulate(fishes: &Vec<i32>, days: i32) -> i64 {
    let mut fish_by_age = [0; 10];

    for fish in fishes {
        fish_by_age[*fish as usize] += 1;
    }

    for _ in 0..days {
        let reset_fishes = fish_by_age[0];
        for age in 1..fish_by_age.len() {
            fish_by_age[age - 1] = fish_by_age[age];
        }
        fish_by_age[6] += reset_fishes;
        fish_by_age[8] = reset_fishes;
    }

    fish_by_age.iter().sum()
}
