use crate::common;

pub fn solve() -> Result<(), String> {
    let input = &common::read_input()[0];
    let bytes = input.as_bytes();

    let mut pos = 0;
    if let Some((packet, packet_end_pos)) = read_packet(&bytes, pos) {
        pos = packet_end_pos;
        println!("solution: {}", sum_packet_versions(&packet));
        println!("interpret: {}", interpret(&packet));
    }

    Ok(())
}

fn sum_packet_versions(packet: &Packet) -> usize {
    match packet {
        Packet::Operator {
            header,
            size,
            packets,
        } => {
            header.version
                + packets
                    .iter()
                    .map(|child| sum_packet_versions(child))
                    .sum::<usize>()
        }
        Packet::LiteralValue { header, value } => header.version,
    }
}

fn interpret(packet: &Packet) -> usize {
    match packet {
        Packet::Operator {
            header,
            size,
            packets,
        } => match header.type_id {
            0 => packets.iter().map(|child| interpret(child)).sum::<usize>(),
            1 => packets
                .iter()
                .map(|child| interpret(child))
                .product::<usize>(),
            2 => packets.iter().map(|child| interpret(child)).min().unwrap(),
            3 => packets.iter().map(|child| interpret(child)).max().unwrap(),
            5 => {
                if interpret(&packets[0]) > interpret(&packets[1]) {
                    1
                } else {
                    0
                }
            }
            6 => {
                if interpret(&packets[0]) < interpret(&packets[1]) {
                    1
                } else {
                    0
                }
            }
            7 => {
                if interpret(&packets[0]) == interpret(&packets[1]) {
                    1
                } else {
                    0
                }
            }
            _ => panic!("unrecognized type: {}", header.type_id),
        },
        Packet::LiteralValue { header, value } => *value,
    }
}

fn read_packet(input: &[u8], mut pos: usize) -> Option<(Packet, usize)> {
    if pos == input.len() * 4 {
        return None;
    }

    if pos > input.len() * 4 {
        panic!("unexpected pos {} for size {}", pos, input.len());
    }

    if input.len() * 4 - pos < 4 {
        return None;
    }

    let header = read_header(input, pos);
    println!("header: {:?}", header);
    pos += 6;

    if header.type_id == 4 {
        let mut value = 0;
        let mut groups = 0;

        loop {
            if read_bit(input, pos + groups * 5) == 0 {
                break;
            }
            groups += 1;
        }
        groups += 1;

        for group in 0..groups {
            let group_value = read_bits(input, pos + 5 * group + 1, pos + 5 * (group + 1));
            println!("group value: {}", group_value);
            value += group_value << (groups - 1 - group) * 4;
        }

        Some((Packet::LiteralValue { header, value }, pos + 5 * groups))
    } else {
        let size = match read_bit(input, pos) {
            0 => OperatorSize::SizeOfAllChildren(read_bits(input, pos + 1, pos + 16)),
            _ => OperatorSize::NumberOfDirectChildren(read_bits(input, pos + 1, pos + 12)),
        };

        println!("{:?}", size);

        let children = match size {
            OperatorSize::NumberOfDirectChildren(num_children) => {
                let mut children = vec![];
                pos = pos + 12;
                for _child in 0..num_children {
                    let (packet, packet_end) = read_packet(input, pos).unwrap();
                    children.push(packet);
                    pos = packet_end;
                }
                children
            }
            OperatorSize::SizeOfAllChildren(size_children) => {
                let mut children = vec![];
                pos = pos + 16;
                let end_pos = pos + size_children;
                while pos < end_pos {
                    let (packet, packet_end) = read_packet(input, pos).unwrap();
                    children.push(packet);
                    pos = packet_end;
                }
                children
            }
        };

        Some((
            Packet::Operator {
                header,
                size,
                packets: children,
            },
            pos,
        ))
    }
}

fn read_header(input: &[u8], pos: usize) -> PacketHeader {
    let version = read_bits(input, pos, pos + 3);
    let type_id = read_bits(input, pos + 3, pos + 6);
    PacketHeader { version, type_id }
}

fn read_bits(input: &[u8], start: usize, end: usize) -> usize {
    let mut result = 0;
    for (n, p) in (start..end).rev().enumerate() {
        let bit = read_bit(input, p);
        result += 2_usize.pow(n as u32) * bit as usize;
    }
    result
}

fn read_bit(input: &[u8], pos: usize) -> u8 {
    let byte_pos = pos / 4;
    let byte = input[byte_pos];
    let num = if byte >= 'A' as u8 && byte <= 'F' as u8 {
        byte - 'A' as u8 + 10
    } else if byte >= '0' as u8 && byte <= '9' as u8 {
        byte - '0' as u8
    } else {
        panic!("unexpected input: {}", byte)
    };

    let bit_pos = 3 - (pos % 4);
    (num >> bit_pos) & 1
}

#[derive(Debug)]
enum Packet {
    LiteralValue {
        header: PacketHeader,
        value: usize,
    },
    Operator {
        header: PacketHeader,
        size: OperatorSize,
        packets: Vec<Packet>,
    },
}

#[derive(Debug)]
struct PacketHeader {
    version: usize,
    type_id: usize,
}

#[derive(Debug)]
enum OperatorSize {
    NumberOfDirectChildren(usize),
    SizeOfAllChildren(usize),
}
