fn read_depths() -> Vec<i32> {
    let input = crate::common::read_input();

    let mut depths = vec![];
    for depth in &input {
        let depth = depth.parse().unwrap();
        depths.push(depth);
    }

    depths
}

pub fn solve() -> Result<(), String> {
    let depths = read_depths();

    let mut previous_depth = 0;
    let mut depth_increased = 0;
    for depth in &depths {
        if *depth > previous_depth {
            depth_increased += 1;
        }
        previous_depth = *depth;
    }

    println!("depth increased: {}", depth_increased - 1);

    let mut previous_depth_window = 0;
    let mut depth_window = 0;
    let mut depth_window_increased = 0;

    for i in 0..depths.len() {
        if i > 3 {
            previous_depth_window -= depths[i - 4];
        }

        if i > 2 {
            depth_window -= depths[i - 3];
        }

        if i > 0 {
            previous_depth_window += depths[i - 1];
        }
        depth_window += depths[i];

        if i > 2 {
            println!("{} {}", depth_window, previous_depth_window);
            if depth_window > previous_depth_window {
                depth_window_increased += 1;
            }
        }
    }

    println!("depth window increased: {}", depth_window_increased);

    Ok(())
}
