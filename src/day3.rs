pub fn solve() -> Result<(), String> {
    let diagnostic_report = crate::common::read_input();
    let diagnostic_size = diagnostic_report[0].len();

    let mut counts = vec![0_i32; diagnostic_size];

    for diagnostic in &diagnostic_report {
        let bytes = diagnostic.as_bytes();
        for index in 0..bytes.len() {
            let bit = bytes[index] - ('0' as u8);
            counts[index] += bit as i32;
        }
    }

    let mut gamma = 0;
    let mut epsilon = 0;
    let report_count = diagnostic_report.len() as i32;

    for index in 0..diagnostic_size {
        let g = (counts[index] > report_count / 2) as usize;
        gamma += g << (diagnostic_size - 1 - index);

        let e = (counts[index] < report_count / 2) as usize;
        epsilon += e << (diagnostic_size - 1 - index);
    }

    let power_consumption = epsilon * gamma;

    println!(
        "gamma {} epsilon {} power consumption {}",
        gamma, epsilon, power_consumption
    );

    let oxygen_generator = find_rating(&diagnostic_report, true);
    let co2_scrubber = find_rating(&diagnostic_report, false);

    let life_support = oxygen_generator * co2_scrubber;
    println!(
        "oxygen generator {} co2 scrubber {} life support {}",
        oxygen_generator, co2_scrubber, life_support
    );

    Ok(())
}

fn find_rating(reports: &Vec<String>, highest_occurence: bool) -> i32 {
    let size = reports[0].len();

    let mut reports = reports.to_owned();

    for index in 0..size {
        let mut ones = vec![];
        let mut zeros = vec![];

        if reports.len() == 1 {
            break;
        }

        for report in reports {
            let bit = report.as_bytes()[index] - '0' as u8;
            if bit > 0 {
                ones.push(report.to_owned());
            } else {
                zeros.push(report.to_owned());
            }
        }

        if highest_occurence {
            if ones.len() >= zeros.len() {
                reports = ones;
            } else {
                reports = zeros;
            }
        } else {
            if ones.len() < zeros.len() {
                reports = ones;
            } else {
                reports = zeros;
            }
        }
    }

    i32::from_str_radix(&reports[0], 2).unwrap()
}
