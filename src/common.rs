use std::io::{self, BufRead};

pub fn read_input() -> Vec<String> {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines();

    let mut result: Vec<String> = vec![];
    while let Some(line) = lines.next() {
        match line {
            Ok(line) => result.push(line),
            Err(e) => eprintln!("failed to read: {}", e),
        };
    }

    result
}
