mod common;

mod day1;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;
mod day16;
mod day17;
mod day18;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;
mod day9;

use std::env;

type Command = fn() -> Result<(), String>;

fn main() -> Result<(), String> {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        return Err(format!("Usage {} [day]", &args[0]));
    }

    decode(&args[1])
        .map(|v| v())
        .unwrap_or(Err(format!("Unknown command: {}", args[1])))
}

fn decode(command: &str) -> Option<Command> {
    match command {
        "1" => Some(day1::solve),
        "2" => Some(day2::solve),
        "3" => Some(day3::solve),
        "4" => Some(day4::solve),
        "5" => Some(day5::solve),
        "6" => Some(day6::solve),
        "7" => Some(day7::solve),
        "8" => Some(day8::solve),
        "9" => Some(day9::solve),
        "10" => Some(day10::solve),
        "11" => Some(day11::solve),
        "12" => Some(day12::solve),
        "13" => Some(day13::solve),
        "14" => Some(day14::solve),
        "15" => Some(day15::solve),
        "16" => Some(day16::solve),
        "17" => Some(day17::solve),
        "18" => Some(day18::solve),
        _ => None,
    }
}
