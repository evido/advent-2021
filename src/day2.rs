enum Command {
    Forward(i32),
    Up(i32),
    Down(i32),
}

fn read_commands() -> Vec<Command> {
    let mut commands = vec![];

    for input in crate::common::read_input() {
        let parts = Vec::from_iter(input.split(" "));
        let amount: i32 = parts[1].parse().unwrap();

        let command = match parts[0] {
            "forward" => Command::Forward(amount),
            "down" => Command::Down(amount),
            "up" => Command::Up(amount),
            _ => panic!("unknown command: {}", parts[0]),
        };

        commands.push(command);
    }

    commands
}

pub fn solve() -> Result<(), String> {
    let commands = read_commands();
    let mut horizontal = 0;
    let mut depth = 0;

    for command in &commands {
        match command {
            Command::Up(val) => depth -= val,
            Command::Down(val) => depth += val,
            Command::Forward(val) => horizontal += val,
        }
    }

    println!(
        "horizontal {} depth {} ({})",
        horizontal,
        depth,
        horizontal * depth
    );

    let mut horizontal = 0;
    let mut depth = 0;
    let mut aim = 0;

    for command in &commands {
        match command {
            Command::Up(val) => {
                aim -= val;
            }
            Command::Down(val) => {
                aim += val;
            }
            Command::Forward(val) => {
                horizontal += val;
                depth += aim * val;
            }
        }
    }

    println!(
        "horizontal {} depth {} ({})",
        horizontal,
        depth,
        horizontal * depth
    );

    Ok(())
}
