use crate::common;

pub fn solve() -> Result<(), String> {
    let input: Vec<[String; 14]> = common::read_input()
        .into_iter()
        .map(|val| to_entry(&val))
        .collect();

    let mut result = 0;
    let mut total = 0;

    for entry in &input {
        for i in 10..14 {
            let len = entry[i].len();
            if len == 2 || len == 3 || len == 4 || len == 7 {
                result += 1;
            }
        }

        total += decode(&entry);
    }

    println!("count of digits: {}", result);
    println!("total: {}", total);

    Ok(())
}

fn to_entry(val: &str) -> [String; 14] {
    let mut entry: [String; 14] = Default::default();

    let values: Vec<String> = val
        .split("|")
        .into_iter()
        .flat_map(|val| {
            val.trim()
                .split(" ")
                .map(|s| s.to_owned())
                .collect::<Vec<String>>()
        })
        .collect();

    for i in 0..entry.len() {
        entry[i] = values[i].clone();
    }

    entry
}

fn decode(entry: &[String; 14]) -> i32 {
    let mut samples: [Vec<String>; 10] = Default::default();
    for sample in &entry[..10] {
        samples[sample.len()].push(sample.clone());
    }

    let mut code: [i32; 7] = [-1; 7];
    let mut inverse: [i32; 7] = [-1; 7];
    let segment_0_locator = sum(&freq_vec(&samples[3]), &freq_vec(&samples[2]), -1);
    let segment_0_pos = find(&segment_0_locator, 1);

    code[segment_0_pos as usize] = 0;
    inverse[0] = segment_0_pos;

    let three_segments = freq_vec(&samples[3]);
    let six_segments = freq_vec(&samples[6]);

    let segment_2_locator = sum(&six_segments, &three_segments, -1);
    let segment_2_pos = find(&segment_2_locator, 1);

    code[segment_2_pos as usize] = 2;
    inverse[2] = segment_2_pos;

    let mut segment_5_locator = three_segments.clone();
    segment_5_locator[segment_2_pos as usize] = 0;
    segment_5_locator[segment_0_pos as usize] = 0;
    let segment_5_pos = find(&segment_5_locator, 1);

    code[segment_5_pos as usize] = 5;
    inverse[5] = segment_5_pos;

    let five_segments = freq_vec(&samples[5]);
    let mut segment_1_3_4_6_locator = sum(&five_segments, &six_segments, 1);
    segment_1_3_4_6_locator[segment_0_pos as usize] = 0;
    segment_1_3_4_6_locator[segment_2_pos as usize] = 0;
    segment_1_3_4_6_locator[segment_5_pos as usize] = 0;

    let segment_1_pos = find(&segment_1_3_4_6_locator, 4);
    let segment_3_pos = find(&segment_1_3_4_6_locator, 5);
    let segment_4_pos = find(&segment_1_3_4_6_locator, 3);
    let segment_6_pos = find(&segment_1_3_4_6_locator, 6);

    code[segment_1_pos as usize] = 1;
    inverse[1] = segment_1_pos;
    code[segment_3_pos as usize] = 3;
    inverse[3] = segment_3_pos;
    code[segment_4_pos as usize] = 4;
    inverse[4] = segment_4_pos;
    code[segment_6_pos as usize] = 6;
    inverse[6] = segment_6_pos;

    let mut digit_codes: [i32; 50000] = [-1; 50000];
    for d in 0..10 {
        let mut digit_code = 1;
        for s in 0..7 {
            if TABLE[s][d] == 1 {
                digit_code *= s + 2;
            }
        }
        digit_codes[digit_code as usize] = d as i32;
    }

    let mut output_value = 0;
    for entry in &entry[10..] {
        let mut digit_code = 1;
        for c in entry.as_bytes() {
            digit_code *= code[*c as usize - 'a' as usize] + 2;
        }
        output_value = output_value * 10 + digit_codes[digit_code as usize];
    }

    println!("output value: {}", output_value);
    output_value
}

const TABLE: [[i32; 10]; 7] = [
    [1, 0, 1, 1, 0, 1, 1, 1, 1, 1],
    [1, 0, 0, 0, 1, 1, 1, 0, 1, 1],
    [1, 1, 1, 1, 1, 0, 0, 1, 1, 1],
    [0, 0, 1, 1, 1, 1, 1, 0, 1, 1],
    [1, 0, 1, 0, 0, 0, 1, 0, 1, 0],
    [1, 1, 0, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 1, 1, 0, 1, 1, 0, 1, 1],
];

fn find(f: &[i32; 7], needle: i32) -> i32 {
    f.iter().position(|h| *h == needle).unwrap() as i32
}

fn freq_vec(v: &Vec<String>) -> [i32; 7] {
    v.iter().map(|v| freq(v)).fold([0; 7], |a, b| -> [i32; 7] {
        let mut c = [0; 7];
        for i in 0..7 {
            c[i] = a[i] + b[i];
        }
        return c;
    })
}

fn sum(left: &[i32; 7], right: &[i32; 7], f: i32) -> [i32; 7] {
    let mut result = [0; 7];
    for i in 0..7 {
        result[i] = left[i] + right[i] * f;
    }
    result
}

fn freq(digit: &str) -> [i32; 7] {
    let mut freq = [0; 7];
    for c in digit.as_bytes() {
        let i = *c as i32 - 'a' as i32;
        freq[i as usize] += 1;
    }
    freq
}
