use crate::common;

pub fn solve() -> Result<(), String> {
    let mut heightmap: Vec<Vec<i32>> = common::read_input()
        .iter()
        .map(|line| line.split("").flat_map(|c| c.parse()).collect())
        .collect();

    let mut low_points = vec![];
    let mut low_points_sum = 0;

    let h = heightmap.len();
    let w = heightmap[0].len();
    for i in 0..h {
        for j in 0..w {
            let c = heightmap[i][j];

            if i > 0 && heightmap[i - 1][j] <= c {
                continue;
            }

            if i < h - 1 && heightmap[i + 1][j] <= c {
                continue;
            }

            if j > 0 && heightmap[i][j - 1] <= c {
                continue;
            }

            if j < w - 1 && heightmap[i][j + 1] <= c {
                continue;
            }

            low_points.push(((low_points.len() + 1) as i32, i as i32, j as i32));
            low_points_sum += c + 1;
        }
    }

    println!("low points: {}", low_points.len());
    println!("low points sum: {}", low_points_sum);

    for (i, lp_i, lp_j) in &low_points {
        heightmap[*lp_i as usize][*lp_j as usize] = -i;

        let mut candidates = vec![
            (i, *lp_i - 1, *lp_j),
            (i, *lp_i + 1, *lp_j),
            (i, *lp_i, *lp_j - 1),
            (i, *lp_i, *lp_j + 1),
        ];

        while let Some((i, c_i, c_j)) = candidates.pop() {
            if c_i < 0 || c_i >= h as i32 {
                continue;
            }

            if c_j < 0 || c_j >= w as i32 {
                continue;
            }

            let c = heightmap[c_i as usize][c_j as usize];

            println!("{} {} {} {}", i, c_i, c_j, c);

            if c < 0 || c == 9 {
                continue;
            }

            heightmap[c_i as usize][c_j as usize] = -i;

            candidates.push((i, c_i - 1, c_j));
            candidates.push((i, c_i + 1, c_j));
            candidates.push((i, c_i, c_j - 1));
            candidates.push((i, c_i, c_j + 1));
        }
    }

    let mut basins = vec![0; low_points.len()];
    for row in &heightmap {
        println!("{:?}", row);
        for col in row {
            if *col < 0 {
                basins[(-*col - 1) as usize] += 1;
            }
        }
    }

    println!("basins: {:?}", basins);
    basins.sort();
    println!("basins: {:?}", basins);
    println!(
        "solution: {}",
        basins.iter().rev().take(3).fold(1, |a, b| a * b)
    );

    Ok(())
}
