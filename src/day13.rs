use crate::common;

pub fn solve() -> Result<(), String> {
    let input = common::read_input();
    let mut iter = input.iter();
    let mut marks = vec![];
    let mut folds = vec![];

    while let Some(line) = iter.next() {
        if line.len() == 0 {
            break;
        }

        let mark: Vec<usize> = line.split(",").flat_map(|v| v.parse()).collect();

        marks.push(mark);
    }

    while let Some(line) = iter.next() {
        let bytes = line.as_bytes();
        let p = bytes.iter().position(|v| *v == '=' as u8).unwrap();

        folds.push((bytes[p - 1], parse_usize(&bytes[p + 1..])));
    }

    println!("marks: {:?}", marks);
    println!("folds: {:?}", folds);

    let (mut max_x, mut max_y) = marks
        .iter()
        .fold((0, 0), |a, b| (a.0.max(b[0]), a.1.max(b[1])));

    max_x += 1;
    max_y += 1;

    println!("max_x: {} max_y: {}", max_x, max_y);

    let mut grid = vec![vec![0; max_x]; max_y];

    for mark in &marks {
        grid[mark[1]][mark[0]] = 1;
    }

    println!("---");
    show_grid(&grid, max_x, max_y);

    for fold in &folds {
        if fold.0 == 'x' as u8 {
            for j in 0..max_y {
                for i in 0..fold.1 {
                    grid[j][i] += grid[j][max_x - 1 - i];
                }
            }
            max_x = fold.1;
        } else {
            for j in 0..fold.1 {
                for i in 0..max_x {
                    grid[j][i] += grid[max_y - 1 - j][i];
                }
            }
            max_y = fold.1;
        }

        println!("---");
        show_grid(&grid, max_x, max_y);
    }

    Ok(())
}

fn show_grid(grid: &Vec<Vec<i32>>, max_x: usize, max_y: usize) {
    let mut dots = 0;
    for row in &grid[..max_y] {
        for cell in &row[..max_x] {
            if *cell == 0 {
                print!(".");
            } else {
                print!("#");
                dots += 1;
            }
        }
        print!("\n");
    }
    println!("dots: {}", dots);
}

fn parse_usize(bytes: &[u8]) -> usize {
    let mut parsed = 0;

    for c in bytes {
        parsed *= 10;
        parsed += (c - '0' as u8) as usize;
    }

    parsed
}
