use crate::common;

pub fn solve() -> Result<(), String> {
    let mut grid: Vec<Vec<i32>> = common::read_input()
        .iter()
        .map(|line| line.split("").flat_map(|cell| cell.parse()).collect())
        .collect();

    let mut total_flashes = 0;
    let mut step = 0;

    loop {
        let mut flashes = vec![];
        for i in 0..10 {
            for j in 0..10 {
                grid[i][j] += 1;
                if grid[i][j] == 10 {
                    flashes.push((i, j));
                }
            }
        }

        while let Some((f_i, f_j)) = flashes.pop() {
            total_flashes += 1;

            for di in 0..3 {
                for dj in 0..3 {
                    if di == 1 && dj == 1 {
                        continue;
                    }

                    let mut c_i = f_i + di;
                    let mut c_j = f_j + dj;

                    if c_i > 0 && c_i < 11 && c_j > 0 && c_j < 11 {
                        c_i -= 1;
                        c_j -= 1;
                        grid[c_i][c_j] += 1;
                        if grid[c_i][c_j] == 10 {
                            flashes.push((c_i, c_j));
                        }
                    }
                }
            }
        }

        let mut all_flashing = true;
        for i in 0..10 {
            for j in 0..10 {
                if grid[i][j] > 9 {
                    grid[i][j] = 0;
                } else {
                    all_flashing = false;
                }
            }
        }

        println!("flashes after {} steps: {}", step + 1, total_flashes);

        if all_flashing {
            println!("all flashing after {} steps", step + 1);
            break;
        }

        step += 1;
    }

    Ok(())
}
