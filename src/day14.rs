use crate::common;

use std::collections::HashMap;

pub fn solve() -> Result<(), String> {
    let input = common::read_input();
    let mut iter = input.iter();

    let mut sequence: Vec<u8> = if let Some(sequence) = iter.next() {
        sequence.as_bytes().into()
    } else {
        return Err("Incomplete input sequence".to_owned());
    };

    let mut original = sequence.clone();

    let rules: HashMap<[u8; 2], u8> = iter
        .skip(1)
        .map(|line| {
            let mut parts = line.split("->").map(|expr| expr.trim());
            let code = parts.next().unwrap().as_bytes();
            let translated = parts.next().unwrap().as_bytes();
            ([code[0], code[1]], translated[0])
        })
        .collect();

    println!("rules: {:?}", rules);

    let steps = 10;
    for step in 0..steps {
        println!("step {}", step);
        let mut new_sequence = vec![];

        for i in 0..sequence.len() - 1 {
            let code = &sequence[i..i + 2];
            println!("code: {:?}", code);
            new_sequence.push(code[0]);
            if let Some(translated) = rules.get(code) {
                new_sequence.push(*translated);
            }
        }

        new_sequence.push(*sequence.last().unwrap());

        sequence = new_sequence;
        show_sequence(&sequence);
    }

    let mut histogram = HashMap::new();
    for i in 0..sequence.len() {
        let code = sequence[i];
        *histogram.entry(code).or_insert(0) += 1;
    }

    let mut sorted: Vec<(u8, i32)> = histogram.into_iter().collect();
    sorted.sort_by(|a, b| a.1.partial_cmp(&b.1).unwrap());

    println!("histogram: {:?}", sorted);
    println!("answer: {}", sorted[sorted.len() - 1].1 - sorted[0].1);

    let mut histogram = HashMap::new();

    for i in 0..original.len() - 1 {
        let code = [original[i], original[i + 1]];
        *histogram.entry(code).or_insert(0i64) += 1;
    }

    let mut single_histogram = HashMap::new();
    for c in original {
        *single_histogram.entry(c).or_insert(0) += 1;
    }

    for step in 0..40 {
        let mut new_histogram = HashMap::new();

        for entry in histogram.iter() {
            if let Some(translated) = rules.get(entry.0) {
                *new_histogram.entry([entry.0[0], *translated]).or_insert(0) += entry.1;
                *new_histogram.entry([*translated, entry.0[1]]).or_insert(0) += entry.1;
                *single_histogram.entry(*translated).or_insert(0) += entry.1;
            }
        }

        histogram = new_histogram;
    }

    let mut sorted: Vec<(u8, i64)> = single_histogram.into_iter().collect();
    sorted.sort_by(|a, b| a.1.partial_cmp(&b.1).unwrap());

    println!("histogram: {:?}", sorted);
    println!("answer: {}", sorted[sorted.len() - 1].1 - sorted[0].1);

    Ok(())
}

fn show_sequence(sequence: &Vec<u8>) {
    let s = String::from_utf8(sequence.clone()).unwrap();
    println!("{:?}", s);
}
